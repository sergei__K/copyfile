package copyFile;

import java.io.*;

/**
 * Created by g15oit18 on 21.12.2017.
 */
public class Flows extends Thread {
    private String readr;
    private String writer;

    public Flows(String readr, String writer) {
        this.readr = readr;
        this.writer = writer;
    }
    public void run() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(readr));
             BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(writer))) {
            String data;
            long timeStart, timeFinish;
            timeStart = System.currentTimeMillis();
            while ((data = bufferedReader.readLine()) != null) {
                bufferedWriter.write(data);
            }
            timeFinish = System.currentTimeMillis();
            System.out.println("Время = " + (timeFinish-timeStart));
        } catch (IOException e) {
            System.out.println("ERROR" + e);
        }
    }
}