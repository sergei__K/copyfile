package copyFile;

/**
 * Created by g15oit18 on 21.12.2017.
 */
public class Main {
    public static void main(String[] args) {
        Flows flows1 = new Flows("src//copyFile//data.txt","src//copyFile//File1.txt");
        Flows flows2 = new Flows("src//copyFile//data.txt","src//copyFile//File2.txt");
        flows1.start();
        flows2.start();
    }
}